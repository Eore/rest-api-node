let findUser = router => {
  router.get("/", (req, res) => {
    console.log(`/user => GET (${req.connection.remoteAddress})`);
    res.send(
      "miaw " + req.connection.remoteAddress + " - " + req.connection.socket
    );
  });

  return router;
};

module.exports = findUser;
