let crypto = require("crypto");
let Connection = require("../../libs/database");

class User {
  constructor({ username, password, email, nama, role }) {
    this.username = username;
    this.password = crypto
      .createHash("sha256")
      .update(password)
      .digest("hex");
    this.email = email;
    this.nama = nama;
    this.role = role;
  }
}

module.exports = User;
